package ru.aston.emailservice.kafka;


import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import ru.aston.emailservice.kafka.dto.AccountEmailMessageDto;
import ru.aston.emailservice.kafka.dto.CardEmailMessageDto;
import ru.aston.emailservice.kafka.dto.UserEmailMessageDto;
import ru.aston.emailservice.service.EmailSenderService;

import java.util.function.Consumer;

@Component
public class KafkaConsumer {

    private final EmailSenderService emailSenderService;

    public KafkaConsumer(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    @Bean
    public Consumer<AccountEmailMessageDto> accountEventEmailMessage() {
        return emailSenderService::sendEventEmail;
    }

    @Bean
    public Consumer<CardEmailMessageDto> cardEventEmailMessage() {
        return emailSenderService::sendEventEmail;
    }

    @Bean
    public Consumer<UserEmailMessageDto> userEventEmailMessage() {
        return emailSenderService::sendEventEmail;
    }
}
