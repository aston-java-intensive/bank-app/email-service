package ru.aston.emailservice.kafka.dto;

public class EmailMessageDto {

    private String userEmail;
    private String event;

    public EmailMessageDto() {
    }

    public EmailMessageDto(String userEmail, String event) {
        this.userEmail = userEmail;
        this.event = event;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
