package ru.aston.emailservice.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.aston.emailservice.kafka.dto.AccountEmailMessageDto;
import ru.aston.emailservice.kafka.dto.CardEmailMessageDto;
import ru.aston.emailservice.kafka.dto.UserEmailMessageDto;

// TODO: 23.04.2023 Отрефакторить метод, вынести в общий интерфес отправку письма
@Service
public class EmailSenderService {

    @Value("${my.mail.username}")
    private static final String MESSAGE_SET_FROM = "poch74.t@yandex.ru";

    private static final String MESSAGE_SUBJECT = "Your %s was %s";

    private static final String ACCOUNT_MESSAGE_BODY =
            "Your %s was %s at %s. Actual currency is %s and status is %s";

    private static final String CARD_MESSAGE_BODY =
            "Your %s number %s was %s. Actual payment system is %s, status is %s, and expiration date is %s";

    private static final String USER_MESSAGE_BODY =
            "Dear %s, your %s was %s at %s. Actual phone number is %s and status is %s";

    private static final String ACCOUNT = "account";

    private static final String CARD = "card";

    private static final String USER = "profile";

    private final JavaMailSender mailSender;

    public EmailSenderService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendSimpleEmail(String toEmail,
                                String subject,
                                String body) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(MESSAGE_SET_FROM);
        message.setTo(toEmail);
        message.setSubject(subject);
        message.setText(body);

        mailSender.send(message);
    }

    public void sendEventEmail(AccountEmailMessageDto emailMessageDto) {
        sendSimpleEmail(
                emailMessageDto.getUserEmail(),
                String.format(MESSAGE_SUBJECT, ACCOUNT, emailMessageDto.getEvent()),
                String.format(
                        ACCOUNT_MESSAGE_BODY,
                        ACCOUNT,
                        emailMessageDto.getEvent(),
                        emailMessageDto.getUpdatedAt(),
                        emailMessageDto.getCurrency(),
                        emailMessageDto.getStatus()
                )
        );
    }

    public void sendEventEmail(CardEmailMessageDto emailMessageDto) {
        sendSimpleEmail(
                emailMessageDto.getUserEmail(),
                String.format(MESSAGE_SUBJECT, CARD, emailMessageDto.getEvent()),
                String.format(
                        CARD_MESSAGE_BODY,
                        CARD,
                        emailMessageDto.getNumber(),
                        emailMessageDto.getEvent(),
                        emailMessageDto.getPaymentSystem(),
                        emailMessageDto.getStatus(),
                        emailMessageDto.getExpDate()
                )
        );
    }

    public void sendEventEmail(UserEmailMessageDto emailMessageDto) {
        sendSimpleEmail(
                emailMessageDto.getUserEmail(),
                String.format(MESSAGE_SUBJECT, USER, emailMessageDto.getEvent()),
                String.format(
                        USER_MESSAGE_BODY,
                        emailMessageDto.getFirstName(),
                        USER,
                        emailMessageDto.getEvent(),
                        emailMessageDto.getUpdatedAt(),
                        emailMessageDto.getPhoneNumber(),
                        emailMessageDto.getStatus()
                )
        );
    }
}

